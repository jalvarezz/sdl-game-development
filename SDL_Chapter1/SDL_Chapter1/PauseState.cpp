#include "MenuState.h"
#include "TextureManager.h"
#include "Game.h"
#include "MenuButton.h"
#include "PauseState.h"
#include "MenuState.h"
#include "InputHandler.h"

const std::string PauseState::s_pauseID = "PAUSE";

void PauseState::update() { 
    for(int i = 0; i < m_gameObjects.size(); i++){
        m_gameObjects[i]->update();
	}

	//Check and deque the state if its invalid
	TheGame::Instance()->getStateMachine()->dequeState();
}

void PauseState::render() { 
	for(int i = 0; i < m_gameObjects.size(); i++){
		m_gameObjects[i]->draw();
	}
}

bool PauseState::onEnter() { 
	if(!TheTextureManager::Instance()->load("Resources/mainmenubutton.png", "mainButton", TheGame::Instance()->getRenderer())){
		return false;
	}

	if(!TheTextureManager::Instance()->load("Resources/resumebutton.png", "resumeButton", TheGame::Instance()->getRenderer())){
		return false;
	}

	GameObject* button1 = new MenuButton(new LoaderParams(250, 200, 159, 60, "mainButton"), s_pauseToMain);
	GameObject* button2 = new MenuButton(new LoaderParams(250, 300, 159, 60, "resumeButton"), s_resumePlay);

	m_gameObjects.push_back(button1);
	m_gameObjects.push_back(button2);

	std::cout << "entering Pause\n";
	return true;
}

bool PauseState::onExit() { 
	for (int i = 0; i < m_gameObjects.size(); i++)
	{
		m_gameObjects[i]->clean();
	}

	m_gameObjects.clear();
	TheTextureManager::Instance()->clearTextureFromMap("mainbutton");
	TheTextureManager::Instance()->clearTextureFromMap("resumebutton");

	// reset the mouse button states to false
	TheInputHandler::Instance()->reset();

	std::cout << "exiting Pause\n";
	return true;
}

void PauseState::s_pauseToMain(){ 
	TheGame::Instance()->getStateMachine()->changeState(new MenuState());
}

void PauseState::s_resumePlay(){ 
	TheGame::Instance()->getStateMachine()->popState();
}