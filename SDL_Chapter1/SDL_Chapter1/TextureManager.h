#include "SDL.h"
#include "SDL_image.h"
#include <iostream>
#include <string>
#include <map>

#ifndef __TextureManager__
#define __TextureManager__

class TextureManager {
public:
	static TextureManager* Instance(){
		if(s_pInstance == 0){
			s_pInstance = new TextureManager();
			return s_pInstance;
		}

		return s_pInstance;
	}

	~TextureManager() { }

	bool load(std::string fileName, std::string id, SDL_Renderer* pRenderer);

	//draw
	void draw(std::string id, int x, int y, int width, int height, SDL_Renderer* pRenderer, SDL_RendererFlip flip = SDL_FLIP_NONE);

	//draw frame
	void drawFrame(std::string id, int x, int y, int width, int height, int currentRow, int currentFrame, SDL_Renderer* pRenderer, SDL_RendererFlip flip = SDL_FLIP_NONE);

	void clearTextureFromMap(std::string id);

private:
	TextureManager() { }

	//Singleton Instance
	static TextureManager* s_pInstance;
	
	//textures map
	std::map<std::string, SDL_Texture*> m_textureMap;
};

typedef TextureManager TheTextureManager;

#endif /* defined(__TextureManager__)*/