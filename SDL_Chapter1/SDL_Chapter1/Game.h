#include "SDL.h"
#include "SDL_image.h"
#include "GameObject.h"
#include "GameStateMachine.h"
#include <iostream>
#include <vector>

#ifndef __Game__
#define __Game__

class Game{
public:
	~Game() { }

	//Gets the current game instance
	static Game* Instance(){
		if(s_pInstance == 0){
			s_pInstance = new Game();
		}

		return s_pInstance;
	}

	// simply set the running variable to true
	bool init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen);

	void render();
	void update();
	void handleEvents();
	void clean();
	void quit();

	//a function to access the private running variable
	bool running() { return m_bRunning; }

	SDL_Renderer* getRenderer() const { return m_pRenderer; }

	GameStateMachine* getStateMachine() { return m_pGameStateMachine; }

private:
	Game() { }

	//Game instance
	static Game* s_pInstance;

	SDL_Window* m_pWindow;
	SDL_Renderer* m_pRenderer;

	int m_currentFrame;
	bool m_bRunning;

	std::vector<GameObject*> m_gameObjects;
	GameStateMachine* m_pGameStateMachine;

	//Draw objects function
	void draw();
};

typedef Game TheGame;

#endif /* defined(__Game__) */