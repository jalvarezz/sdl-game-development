#include "Player.h"
#include "InputHandler.h"

Player::Player(const LoaderParams* pParams) : SDLGameObject(pParams){ 
}

void Player::draw(){
	SDLGameObject::draw(); // we now use SDLGameObject
}

void Player::update()
{
	handleInput(); // add our function

	m_currentFrame = int(((SDL_GetTicks() / 100) % m_numFrames));

	SDLGameObject::update();
}

void Player::clean() { }

void Player::handleInput() {
	//MOUSE
	Vector2D* target = TheInputHandler::Instance()->getMousePosition();
	m_velocity = *target - m_position;
	m_velocity /= 50;
}