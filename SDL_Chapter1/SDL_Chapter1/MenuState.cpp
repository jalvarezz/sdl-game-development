#include "MenuState.h"
#include "TextureManager.h"
#include "Game.h"
#include "MenuButton.h"
#include "PlayState.h"

const std::string MenuState::s_menuID = "MENU";

void MenuState::update() { 
    for(int i = 0; i < m_gameObjects.size(); i++){
        m_gameObjects[i]->update();
    }

	//Check and deque the state if its invalid
	TheGame::Instance()->getStateMachine()->dequeState();
}

void MenuState::render() { 
	for(int i = 0; i < m_gameObjects.size(); i++){
		m_gameObjects[i]->draw();
	}
}

bool MenuState::onEnter() { 
	if(!TheTextureManager::Instance()->load("Resources/playbutton.png", "playButton", TheGame::Instance()->getRenderer())){
		return false;
	}

	if(!TheTextureManager::Instance()->load("Resources/exitbutton.png", "exitButton", TheGame::Instance()->getRenderer())){
		return false;
	}

	GameObject* button1 = new MenuButton(new LoaderParams(250, 200, 159, 60, "playButton"), s_menuToPlay);
	GameObject* button2 = new MenuButton(new LoaderParams(250, 300, 159, 60, "exitButton"), s_exitFromMenu);

	m_gameObjects.push_back(button1);
	m_gameObjects.push_back(button2);

	std::cout << "entering MenuState\n";
	return true;
}

bool MenuState::onExit() { 
	for (int i = 0; i < m_gameObjects.size(); i++)
	{
		m_gameObjects[i]->clean();
	}

	m_gameObjects.clear();
	TheTextureManager::Instance()->clearTextureFromMap("playButton");
	TheTextureManager::Instance()->clearTextureFromMap("exitButton");

	std::cout << "exiting MenuState\n";
	return true;
}

void MenuState::s_menuToPlay(){
	TheGame::Instance()->getStateMachine()->changeState(new PlayState());
}

void MenuState::s_exitFromMenu(){
	TheGame::Instance()->quit();
}

