#include <iostream>
#include "SDLGameObject.h"

#ifndef __Player__
#define __Player__

class Player : public SDLGameObject /*Derived from SDLGameObject*/
{
public:
	Player(const LoaderParams* pParams);

	void draw();
	void update();
	void clean();

private:

	void handleInput();
};

#endif /* Defined(__Player__) */