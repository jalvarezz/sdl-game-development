#include "SDL.h"
#include "SDL_joystick.h"
#include <iostream>
#include <vector>
#include "Vector2D.h"

#ifndef __InputHandler__
#define __InputHandler__

const int m_joystickDeadZone = 30000;

enum mouse_buttons{
	LEFT = 0,
	MIDDLE = 1,
	RIGHT = 2
};

class InputHandler {
public:
	static InputHandler* Instance(){
		if(s_pInstance == 0){
			s_pInstance = new InputHandler();
		}

		return s_pInstance;
	}

	void update();
	void clean();
	void initialiseJoysticks();

	int xvalue(int joy, int stick);
	int yvalue(int joy, int stick);

	bool joysticksInitialised() {
		return m_bJoysticksInitialised;
	}

	bool getButtonState(int joy, int buttonNumber){
		return m_buttonStates[joy][buttonNumber];
	}

	bool getMouseButtonState(int buttonNumber){
		return m_mouseButtonStates[buttonNumber];
	}

	Vector2D* getMousePosition(){
		return m_mousePosition;
	}

	bool isKeyDown(SDL_Scancode key){
		if(m_keystates != 0){
			if(m_keystates[key] == 1){
				return true;
			} else {
				return false;
			}
		}

		return false;
	}

	void reset();

private:
	InputHandler() { 
		for (int i = 0; i < 3; i++)
		{
			m_mouseButtonStates.push_back(false);
		}

		m_mousePosition = new Vector2D(0,0);
	}

	~InputHandler() { }

	static InputHandler* s_pInstance;

	std::vector<SDL_Joystick*> m_joysticks;
	std::vector<std::pair<Vector2D*, Vector2D*>> m_joystickValues;

	std::vector<std::vector<bool>> m_buttonStates;
	std::vector<bool> m_mouseButtonStates;

	Vector2D* m_mousePosition;

	const Uint8* m_keystates;

	bool m_bJoysticksInitialised;

	// private functions to handle different event types
	
	// handle keyboard events
	void onKeyDown();
	void onKeyUp();

	// handle mouse events
	void onMouseMove(SDL_Event &event);
	void onMouseButtonDown(SDL_Event &event);
	void onMouseButtonUp(SDL_Event &event);

	// handle joystick events
	void onJoystickAxisMove(SDL_Event &event);
	void onJoystickButtonDown(SDL_Event &event);
	void onJoystickButtonUp(SDL_Event &event);
};

typedef InputHandler TheInputHandler;

#endif /*defined(__InputHandler__)*/