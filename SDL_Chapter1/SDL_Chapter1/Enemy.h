#include <iostream>
#include "SDLGameObject.h"

#ifndef __Enemy__
#define __Enemy__

class Enemy : public SDLGameObject /*Derived from SDLGameObject*/
{
public:
	Enemy(const LoaderParams* pParams);

	void draw();
	void update();
	void clean();
};

#endif /* Defined(__Enemy__) */