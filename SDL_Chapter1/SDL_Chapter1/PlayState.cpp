#include "PlayState.h"
#include "TextureManager.h"
#include "Game.h"
#include "MenuButton.h"
#include "Player.h"
#include "Enemy.h"
#include "PauseState.h"
#include "GameOverState.h"
#include "InputHandler.h"

const std::string PlayState::s_playID = "PLAY";

void PlayState::update() { 
	if(TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_ESCAPE)){
		TheGame::Instance()->getStateMachine()->pushState(new PauseState());
	}

	for(int i = 0; i < m_gameObjects.size(); i++){
        m_gameObjects[i]->update();
	}

	if(checkCollision(dynamic_cast<SDLGameObject*>(m_gameObjects[0]), dynamic_cast<SDLGameObject*>(m_gameObjects[1]))){
		TheGame::Instance()->getStateMachine()->changeState(new GameOverState());
	}

	//Check and deque the state if its invalid
	TheGame::Instance()->getStateMachine()->dequeState();
}

void PlayState::render() { 
	for(int i = 0; i < m_gameObjects.size(); i++)
	{
		m_gameObjects[i]->draw();
	}
}

bool PlayState::onEnter() { 
	if(!TheTextureManager::Instance()->load("Resources/helicopter.png", "helicopter", TheGame::Instance()->getRenderer())){
		return false;
	}

	if(!TheTextureManager::Instance()->load("Resources/helicopter2.png", "helicopter2", TheGame::Instance()->getRenderer())){
		return false;
	}

	GameObject* player = new Player(new LoaderParams(500, 100, 132, 52, "helicopter", 4));
	GameObject* enemy = new Enemy(new LoaderParams(100, 100, 132, 52, "helicopter2", 4));

	m_gameObjects.push_back(player);
	m_gameObjects.push_back(enemy);

	std::cout << "entering PlayState\n";
	return true;
}

bool PlayState::onExit() { 

	m_gameObjects.clear();
	TheTextureManager::Instance()->clearTextureFromMap("helicopter");

	std::cout << "exiting PlayState\n";
	return true;
}

bool PlayState::checkCollision(SDLGameObject* p1, SDLGameObject* p2){
	int leftA, leftB;
	int rightA, rightB;
	int topA, topB;
	int bottomA, bottomB;

	//Calculate the sides of rect A
	leftA = p1->getPosition().getX();
	rightA = p1->getPosition().getX() + p1->getWidth();
	topA = p1->getPosition().getY();
	bottomA = p1->getPosition().getY() + p1->getHeight();

	//Calculate the sides of rect B
	leftB = p2->getPosition().getX();
	rightB = p2->getPosition().getX() + p2->getWidth();
	topB = p2->getPosition().getY();
	bottomB = p2->getPosition().getY() + p2->getHeight();

	//If any of the sides from A are outside of B
	if(bottomA <= topB || topA >= bottomB || leftA >= rightB || rightA <= leftB){
		return false;
	}

	return true;
}