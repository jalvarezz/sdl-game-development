#include "GameState.h"
#include "GameObject.h"
#include <iostream>
#include <string>
#include <vector>
#include "SDLGameObject.h"

#ifndef __PlayState__
#define __PlayState__

class PlayState : public GameState
{
public:
	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();
		
	virtual std::string getStateID() const { return s_playID; }

private:
	static const std::string s_playID;

	std::vector<GameObject*> m_gameObjects;

	bool checkCollision(SDLGameObject* p1, SDLGameObject* p2);
};

#endif /* defined(__PlayState__) */