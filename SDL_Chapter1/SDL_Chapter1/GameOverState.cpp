#include "GameOverState.h"
#include "Game.h"
#include "MenuState.h"
#include "PlayState.h"
#include "TextureManager.h"
#include "MenuButton.h"
#include "AnimatedGraphic.h"

const std::string GameOverState::s_gameOverID = "GAMEOVER";

void GameOverState::update() { 
    for(int i = 0; i < m_gameObjects.size(); i++){
        m_gameObjects[i]->update();
    }

	//Check and deque the state if its invalid
	TheGame::Instance()->getStateMachine()->dequeState();
}

void GameOverState::render() { 
	for(int i = 0; i < m_gameObjects.size(); i++)
	{
		m_gameObjects[i]->draw();
	}
}

bool GameOverState::onEnter() { 
	if(!TheTextureManager::Instance()->load("Resources/gameover.png", "gameovertext", TheGame::Instance()->getRenderer())){
		return false;
	}

	if(!TheTextureManager::Instance()->load("Resources/mainmenubutton.png", "mainButton", TheGame::Instance()->getRenderer())){
		return false;
	}

	if(!TheTextureManager::Instance()->load("Resources/restartbutton.png", "restartButton", TheGame::Instance()->getRenderer())){
		return false;
	}

	GameObject* gameOverText = new AnimatedGraphic(new LoaderParams(270, 50, 124, 132, "gameovertext", 1), 1);
	GameObject* button1 = new MenuButton(new LoaderParams(250, 200, 159, 60, "mainButton"), s_gameOverToMain);
	GameObject* button2 = new MenuButton(new LoaderParams(250, 300, 159, 60, "restartButton"), s_restartPlay);

	m_gameObjects.push_back(gameOverText);
	m_gameObjects.push_back(button1);
	m_gameObjects.push_back(button2);

	std::cout << "entering MenuState\n";
	return true;
}

bool GameOverState::onExit() { 
	for (int i = 0; i < m_gameObjects.size(); i++)
	{
		m_gameObjects[i]->clean();
	}

	m_gameObjects.clear();
	TheTextureManager::Instance()->clearTextureFromMap("playButton");
	TheTextureManager::Instance()->clearTextureFromMap("exitButton");

	std::cout << "exiting MenuState\n";
	return true;
}

void GameOverState::s_gameOverToMain(){
	TheGame::Instance()->getStateMachine()->changeState(new MenuState());
}

void GameOverState::s_restartPlay(){
	TheGame::Instance()->getStateMachine()->changeState(new PlayState());
}