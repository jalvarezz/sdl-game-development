#include "GameState.h"
#include "InputHandler.h"
#include <iostream>
#include <vector>

#ifndef __GameStateMachine__
#define __GameStateMachine__

class GameStateMachine
{
public:
	void pushState(GameState* pState);
	void changeState(GameState* pState);
	void popState();
	void dequeState();

	void update();
	void render();

private:
	std::vector<GameState*> m_gameStates;
};

#endif /* defined(__GameStateMachine__) */