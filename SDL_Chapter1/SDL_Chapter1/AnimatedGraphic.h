#include <iostream>
#include "SDLGameObject.h"

#ifndef __AnimatedGraphic__
#define __AnimatedGraphic__

class AnimatedGraphic : public SDLGameObject /*Derived from SDLGameObject*/
{
public:
	AnimatedGraphic(const LoaderParams* pParams, int animSpeed);

	void draw();
	void update();
	void clean();

private:
	int m_animSpeed;

	void handleInput();
};

#endif /* Defined(__AnimatedGraphic__) */